package br.com.enjoei.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import br.com.enjoei.R;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by anndressa on 12/02/15.
 */
public class FormEditText extends FrameLayout {
    @InjectView(R.id.label)
    TextView label;
    @InjectView(R.id.obs)
    TextView obs;
    @InjectView(R.id.edittext)
    EditText editText;

    public FormEditText(Context context) {
        super(context);
        init(null);
    }

    public FormEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public FormEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FormEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    public void init(AttributeSet attrs) {
        View.inflate(getContext(), R.layout.view_form_edittext, this);
        ButterKnife.inject(this, this);

        TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.FormEditText, 0, 0);
        try {
            label.setText(a.getText(R.styleable.FormEditText_formLabel));
            obs.setText(a.getText(R.styleable.FormEditText_formObs));
            editText.setHint(a.getText(R.styleable.FormEditText_formHint));
        } finally {
            a.recycle();
        }
    }

    public EditText getEditText() {
        return editText;
    }

    public TextView getLabel() {
        return label;
    }

    public TextView getObs() {
        return obs;
    }

    public String getText() {
        return editText.getText().toString();
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        return new SavedState(super.onSaveInstanceState(), editText.onSaveInstanceState(), label.onSaveInstanceState(), obs.onSaveInstanceState());
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        editText.onRestoreInstanceState(savedState.getEditTextViewState());
        label.onRestoreInstanceState(savedState.getLabelTextViewState());
        obs.onRestoreInstanceState(savedState.getObsTextViewState());
    }

    @Override
    protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
        super.dispatchFreezeSelfOnly(container);
    }

    @Override
    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
        super.dispatchThawSelfOnly(container);
    }

    protected static class SavedState extends BaseSavedState {
        private Parcelable editTextViewState;
        private Parcelable labelTextViewState;
        private Parcelable obsTextViewState;

        public SavedState(Parcelable superState, Parcelable editTextViewState, Parcelable labelTextViewState, Parcelable obsTextViewState) {
            super(superState);
            this.editTextViewState = editTextViewState;
            this.labelTextViewState = labelTextViewState;
            this.obsTextViewState = obsTextViewState;
        }

        private SavedState(Parcel in) {
            super(in);
            this.editTextViewState = in.readParcelable(TextView.SavedState.class.getClassLoader());
            this.labelTextViewState = in.readParcelable(TextView.SavedState.class.getClassLoader());
            this.obsTextViewState = in.readParcelable(TextView.SavedState.class.getClassLoader());
        }

        public Parcelable getEditTextViewState() {
            return editTextViewState;
        }

        public Parcelable getLabelTextViewState() {
            return labelTextViewState;
        }

        public Parcelable getObsTextViewState() {
            return obsTextViewState;
        }

        @Override
        public void writeToParcel(Parcel destination, int flags) {
            super.writeToParcel(destination, flags);
            destination.writeParcelable(this.editTextViewState, flags);
            destination.writeParcelable(this.labelTextViewState, flags);
            destination.writeParcelable(this.obsTextViewState, flags);
        }

        public static final Parcelable.Creator<SavedState> CREATOR = new Creator<SavedState>() {

            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }

        };

    }
}
