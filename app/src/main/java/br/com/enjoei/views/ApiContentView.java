package br.com.enjoei.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import br.com.enjoei.R;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by anndressa on 14/02/15.
 */
public class ApiContentView extends FrameLayout {
    @InjectView(R.id.loading_view)
    View loadingView;
    @InjectView(R.id.error_view)
    View errorView;
    @InjectView(R.id.error_message)
    TextView errorMessageTextView;

    @InjectView(R.id.content_view)
    SwipeRefreshLayout swipeContentView;

    public ApiContentView(Context context) {
        super(context);
        init(null);
    }

    public ApiContentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ApiContentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ApiContentView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    public void init(AttributeSet attrs) {
        View.inflate(getContext(), R.layout.view_api_content, this);
        ButterKnife.inject(this, this);
    }

    public void setContentView(int resLayout) {
        setContentView(View.inflate(getContext(), resLayout, null));
    }

    public void setContentView(View contentView) {
        this.swipeContentView.removeAllViews();
        this.swipeContentView.addView(contentView);
    }

    public void setMessageError(CharSequence errorMessage) {
        this.errorMessageTextView.setText(errorMessage);
    }

    SwipeRefreshLayout.OnRefreshListener onRefreshListener;

    public void setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener onRefreshListener) {
        this.onRefreshListener = onRefreshListener;
        swipeContentView.setOnRefreshListener(onRefreshListener);
    }

    @OnClick(R.id.error_button)
    public void tryAgain() {
        if (onRefreshListener != null)
            onRefreshListener.onRefresh();
    }

    protected void show(View view) {
        loadingView.setVisibility(View.GONE);
        errorView.setVisibility(View.GONE);
        swipeContentView.setVisibility(View.GONE);
        view.setVisibility(View.VISIBLE);
    }

    public void showLoadingView() {
        show(loadingView);
    }

    public void showErrorView() {
        show(errorView);
    }

    public void showContentView() {
        show(swipeContentView);
    }

    public SwipeRefreshLayout getSwipeContentView() {
        return swipeContentView;
    }
}
