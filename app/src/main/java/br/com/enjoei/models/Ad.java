package br.com.enjoei.models;

import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anndressa on 12/02/15.
 */
@Table(name = "ads")
public class Ad extends Model {
    @Column(name = "remote_id")
    @SerializedName("id")
    public long remoteId;

    @Column(name = "images")
    public String savedImages;

    public String[] images;

    @Column(name = "title")
    public String title;
    @Column(name = "description")
    public String description;
    @Column(name = "price")
    public double price;
    @Column(name = "likes")
    public int likes;
    @Column(name = "category")
    public int category;

    public User user;

    @Override
    public void save() {
        savedImages = TextUtils.join(",", images);
        super.save();
    }
}
