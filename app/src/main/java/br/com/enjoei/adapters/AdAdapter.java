package br.com.enjoei.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.enjoei.R;
import br.com.enjoei.helpers.RoundedTransformation;
import br.com.enjoei.models.Ad;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by anndressa on 14/02/15.
 */
public class AdAdapter extends BaseAdapter {
    List<Ad> items = new ArrayList<>();

    public void addAll(List<Ad> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Ad getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        Ad ad = getItem(position);
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.item_ad, null);
            viewHolder = new ViewHolder(convertView);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Picasso.with(context).load(ad.images[0]).into(viewHolder.adImageView);
        viewHolder.adTitleView.setText(ad.title);
        viewHolder.adPriceView.setText(String.format("R$ %.2f", ad.price));
        Picasso.with(context).load(ad.user.avatar).transform(new RoundedTransformation()).into(viewHolder.adUserAvatarView);
        viewHolder.adLikesView.setText(ad.likes + "");

        return convertView;
    }

    static class ViewHolder {
        @InjectView(R.id.ad_image)
        ImageView adImageView;
        @InjectView(R.id.ad_title)
        TextView adTitleView;
        @InjectView(R.id.ad_price)
        TextView adPriceView;
        @InjectView(R.id.ad_user_avatar)
        ImageView adUserAvatarView;
        @InjectView(R.id.ad_num_likes)
        TextView adLikesView;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
            view.setTag(this);
        }
    }
}
