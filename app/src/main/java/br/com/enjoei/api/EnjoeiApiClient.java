package br.com.enjoei.api;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import br.com.enjoei.R;
import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by anndressa on 14/02/15.
 */
public class EnjoeiApiClient {
    static private EnjoeiApiService apiService;

    public static EnjoeiApiService getApiService() {
        if (apiService == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(EnjoeiApiService.API_URL)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();

            apiService = restAdapter.create(EnjoeiApiService.class);
        }
        return apiService;
    }

}
