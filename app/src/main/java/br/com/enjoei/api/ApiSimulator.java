package br.com.enjoei.api;

import android.text.TextUtils;
import android.util.Log;

import com.activeandroid.query.Select;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.enjoei.EnjoeiApplication;
import br.com.enjoei.helpers.ImageHelper;
import br.com.enjoei.models.Ad;
import br.com.enjoei.models.User;
import retrofit.Callback;
import retrofit.android.MainThreadExecutor;

/**
 * Created by anndressa on 14/02/15.
 */
public class ApiSimulator {

    static final String[] users = {
            "https://res.cloudinary.com/enjoei/image/upload/c_crop,h_2001,w_2002,x_111,y_40/c_thumb,f_auto,h_50,w_50/kl5a5iltawwcxsvc5wh0.jpg",
            "https://res.cloudinary.com/enjoei/image/upload/c_crop,h_2001,w_2002,x_111,y_40/c_thumb,f_auto,h_50,w_50/ypantrkjasfeozneqjcz.jpg"
    };
    static final String[] images = {
            "https://res.cloudinary.com/enjoei/image/upload/c_fit,f_auto,h_1200,q_80,w_1200/r8vgfi6vjd9sew0vjmwq.jpg",
            "https://res.cloudinary.com/enjoei/image/upload/c_fit,f_auto,h_1200,q_80,w_1200/wujise5un65qbzuvcv5f.jpg",
            "https://res.cloudinary.com/enjoei/image/upload/c_fit,f_auto,h_1200,q_80,w_1200/wdstzyii5focqpvzrl3c.jpg"
    };
    public static void listAds(final int page,
                        final Callback<List<Ad>> callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    List<Ad> list = new ArrayList<Ad>();

                    List<Ad> search = new Select().all().from(Ad.class).execute();
                    for(Ad item : search){
                        item.images = TextUtils.split(item.savedImages, ",");
                        item.user = new User();
                        item.user.avatar = users[0];
                        list.add(item);
                    }

                    for(int i=0; i<6; i++){
                        Ad ad = new Ad();
                        ad.title="Anuncio "+i;
                        ad.likes = i*13;
                        ad.price = i*5.5;
                        ad.images = new String[1];
                        ad.images[0] = images[i%3];
                        ad.user = new User();
                        ad.user.avatar = users[i%2];
                        list.add(ad);
                    }

                    final List<Ad> listFinal = list;
                    sleep(1500);
                    new MainThreadExecutor().execute(new Runnable() {
                        @Override
                        public void run() {
                            callback.success(listFinal, null);
                        }
                    });
                } catch (Exception e) {
                    Log.e(EnjoeiApplication.TAG, e.getMessage(), e);
                    new MainThreadExecutor().execute(new Runnable() {
                        @Override
                        public void run() {
                            callback.failure(null);
                        }
                    });
                }
            }
        }.start();
    }

    public static void createAds(final HashMap<String, Object> params,
                                 final Callback<Ad> callback) {
        new Thread() {
            @Override
            public void run() {
                try {
                    Ad ad = new Ad();
                    ad.title = params.get("title").toString();
                    ad.description = params.get("description").toString();
                    ad.price = Double.parseDouble(params.get("price").toString());

                    ArrayList<String> images = new ArrayList<String>();
                    int i = 1;
                    Object param = params.get("image_" + i);
                    while (param != null) {
                        if (param instanceof CountingTypedFile) {
                            CountingTypedFile typedFile = (CountingTypedFile) param;

                            File file = new File(ImageHelper.getTemporaryFilePath());
                            OutputStream out = new FileOutputStream(file);
                            typedFile.writeTo(out);
                            sleep(1500);
                            images.add("file:///" + file);
                        }
                        param = params.get("image_" + (++i));
                    }
                    ad.images = new String[images.size()];
                    images.toArray(ad.images);
                    ad.save();

                    final Ad adFinal = ad;
                    new MainThreadExecutor().execute(new Runnable() {
                        @Override
                        public void run() {
                            callback.success(adFinal, null);
                        }
                    });
                } catch (Exception e) {
                    new MainThreadExecutor().execute(new Runnable() {
                        @Override
                        public void run() {
                            callback.failure(null);
                        }
                    });
                }
            }
        }.start();
    }
}
