package br.com.enjoei.api;

import java.util.HashMap;
import java.util.List;

import br.com.enjoei.models.Ad;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PartMap;
import retrofit.http.Query;

/**
 * Created by anndressa on 12/02/15.
 */
public interface EnjoeiApiService {
    public static final String BASE_API_URL = "http://localhost/api";
    public static final String API_URL = BASE_API_URL + "/v1";

    @GET("/ads")
    public void listAds(
            @Query("page") int page,
            Callback<List<Ad>> callback);

    @Multipart
    @POST("/ads")
    public void createAds(
            @PartMap HashMap<String, Object> params,
            Callback<Ad> callback);
}
