package br.com.enjoei.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import retrofit.android.MainThreadExecutor;
import retrofit.mime.TypedFile;

/**
 * Created by anndressa on 14/02/15.
 */
public class CountingTypedFile extends TypedFile {
    public static interface CountingTypedFileProgressListener {
        void transferred(CountingTypedFile typedFile, long num, long totalSize);
    }

    private static final int BUFFER_SIZE = 4096;

    private final CountingTypedFileProgressListener listener;

    public CountingTypedFile(String mimeType, File file, CountingTypedFileProgressListener listener) {
        super(mimeType, file);
        this.listener = listener;
    }

    @Override
    public void writeTo(OutputStream out) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        FileInputStream in = new FileInputStream(super.file());
        long total = 0;
        try {
            int read;
            while ((read = in.read(buffer)) != -1) {
                total += read;
                out.write(buffer, 0, read);

                new MainThreadExecutor().execute(new ListenerRunnable(total));
            }
        } finally {
            in.close();
        }
    }

    public class ListenerRunnable implements Runnable {
        long total;

        public ListenerRunnable(long total) {
            this.total = total;
        }

        @Override
        public void run() {
            if (listener != null)
                listener.transferred(CountingTypedFile.this, total, length());
        }
    }
}
