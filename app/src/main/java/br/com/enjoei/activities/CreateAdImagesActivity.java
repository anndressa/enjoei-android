package br.com.enjoei.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.enjoei.EnjoeiApplication;
import br.com.enjoei.R;
import br.com.enjoei.helpers.ImageHelper;
import br.com.enjoei.views.CameraPreview;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by anndressa on 12/02/15.
 */
public class CreateAdImagesActivity extends ActionBarActivity implements
        PictureCallback, AutoFocusCallback {

    private static final int PICK_IMAGE_REQUEST = 1;
    public static final String AD_IMAGES_PARAM = "ad_images";

    private Camera mCamera;
    private CameraPreview mPreview;

    @InjectView(R.id.camera_preview)
    FrameLayout container;

    ArrayList<String> imageFilePaths = new ArrayList<>(5);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_ad_images);
        ButterKnife.inject(this);
    }

    @Override
    protected void onPause() {
        stopCamera();
        super.onPause();
    }

    @Override
    protected void onResume() {
        stopCamera();
        startCamera();
        super.onResume();
    }

    private Camera getCameraInstance() {
        Camera camera = null;
        try {
            camera = Camera.open();
            setUpCamera();
        } catch (Exception e) {
            Log.e(EnjoeiApplication.TAG, "Camera exception: " + e.getMessage(),e);
        }
        return camera;
    }

    public void startCamera() {
        mPreview = new CameraPreview(this, this);
        container.addView(mPreview, 0);
        mCamera = getCameraInstance();
        mPreview.setCamera(mCamera);
    }

    public void stopCamera() {
        if (mCamera != null) {
            mPreview.setCamera(null);
            mCamera.cancelAutoFocus();
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
        container.removeView(mPreview);
    }

    public void setUpCamera() {
        if (mCamera != null) {
            Parameters parameters = mCamera.getParameters();
            List<String> focusModes = parameters.getSupportedFocusModes();
            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                parameters.setFocusMode(Parameters.FOCUS_MODE_AUTO);
                mCamera.setParameters(parameters);
            }
        }
    }

    public void addImage(String imageFilePath) {
        startCamera();

        if(imageFilePath == null) return;

        if (imageFilePaths.size() == 5) {
            Toast.makeText(this, R.string.max_images, Toast.LENGTH_SHORT).show();
            return;
        }

        imageFilePaths.add(imageFilePath);

        int idImageView = getResources().getIdentifier("ad_image" + imageFilePaths.size(), "id", getPackageName());
        ImageView imageView = (ImageView) findViewById(idImageView);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Bitmap bitmap = ImageHelper.decodeBitmapFromFile(imageFilePath, 640, 640);
        imageView.setImageBitmap(bitmap);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICK_IMAGE_REQUEST:
                if (data != null && data.getData() != null) {
                    Uri imageUri = data.getData();
                    Bitmap bitmap = ImageHelper.decodeBitmapFromFile(ImageHelper.getAbsoluteImagePathFromUri(this,imageUri), 640, 640);
                    addImage(ImageHelper.saveTemporaryImage(bitmap));
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.gallery)
    public void openGallery() {
        stopCamera();
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.message_gallery)), PICK_IMAGE_REQUEST);
    }

    @OnClick(R.id.take_image)
    public void takeImage() {
        if (mCamera != null) {
            Parameters params = mCamera.getParameters();

            List<String> focusModes = params.getSupportedFocusModes();
            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                mCamera.cancelAutoFocus();
                mCamera.autoFocus(new AutoFocusCallback() {
                    @Override
                    public void onAutoFocus(boolean success, Camera camera) {
                        mCamera.takePicture(null, null, CreateAdImagesActivity.this);
                    }
                });
            } else {
                mCamera.takePicture(null, null, CreateAdImagesActivity.this);
            }
        }
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        stopCamera();

        Bitmap bmp = ImageHelper.decodeSampledBitmapFromResource(data);
        if (bmp != null) {
            String path = ImageHelper.saveTemporaryImage(bmp);
            addImage(path);
        } else {
            startCamera();
            return;
        }
    }

    @Override
    public void onAutoFocus(boolean success, Camera camera) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_ad_images, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_next) {
            if(imageFilePaths.size()>=1){
                startActivity(new Intent(this, CreateAdDetailsActivity.class).putStringArrayListExtra(AD_IMAGES_PARAM,imageFilePaths));
            }else{
                Toast.makeText(this, R.string.min_images, Toast.LENGTH_SHORT).show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}