package br.com.enjoei.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import br.com.enjoei.R;
import br.com.enjoei.api.ApiSimulator;
import br.com.enjoei.api.CountingTypedFile;
import br.com.enjoei.api.EnjoeiApiClient;
import br.com.enjoei.helpers.ApiHelper;
import br.com.enjoei.models.Ad;
import br.com.enjoei.views.FormEditText;
import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CreateAdDetailsActivity extends ActionBarActivity implements CountingTypedFile.CountingTypedFileProgressListener, Callback<Ad> {

    @InjectView(R.id.ad_title)
    FormEditText title;

    @InjectView(R.id.ad_price)
    FormEditText price;

    @InjectView(R.id.ad_description)
    FormEditText description;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_ad_details);
        ButterKnife.inject(this);
        setupViews();
    }

    protected void setupViews() {
        title.getEditText().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_SHORT_MESSAGE);
        description.getEditText().setLines(4);
        price.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(false);
        progressDialog.setMessage(getString(R.string.sending));
        progressDialog.setMax(100);
        progressDialog.setProgress(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_ad_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            if (validate()) {
                sendForm();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected boolean validate() {
        if (title.getText().length() < 5) {
            title.getEditText().setError(getString(R.string.ad_title_obs));
            title.getEditText().requestFocus();
            return false;
        } else {
            title.getEditText().setError(null);
        }

        try {
            double priceValue = Double.parseDouble(price.getText());
            if (priceValue < 0) {
                price.getEditText().setError(getString(R.string.ad_price_invalid));
                price.getEditText().requestFocus();
                return false;
            }
        } catch (NumberFormatException ex) {
            price.getEditText().setError(getString(R.string.ad_price_invalid));
            price.getEditText().requestFocus();
            return false;
        }

        if (description.getText().length() < 25) {
            description.getEditText().setError(getString(R.string.ad_description_obs));
            description.getEditText().requestFocus();
            return false;
        }

        return true;
    }

    protected void sendForm() {
        progressDialog.setProgress(0);
        progressDialog.show();
        EnjoeiApiClient.getApiService().createAds(getAdParams(), this);
    }

    HashMap<CountingTypedFile, Integer> attachments = new HashMap<>();

    protected HashMap<String, Object> getAdParams() {
        HashMap<String, Object> params = new HashMap<String, Object>();

        ArrayList<String> imageFilePaths = getIntent().getExtras().getStringArrayList(CreateAdImagesActivity.AD_IMAGES_PARAM);
        int i = 1;
        for (String imageFilePath : imageFilePaths) {
            CountingTypedFile attachment = new CountingTypedFile("image/jpeg", new File(imageFilePath), this);
            attachments.put(attachment, i);
            params.put("image_" + i++, attachment);
        }

        params.put("title", title.getEditText().getText().toString());
        params.put("price", Double.parseDouble(price.getEditText().getText().toString()));
        params.put("description", description.getEditText().getText().toString());
        return params;
    }

    @Override
    public void success(Ad ad, Response response) {
        progressDialog.dismiss();
        NavUtils.navigateUpFromSameTask(this);
    }

    @Override
    public void failure(RetrofitError error) {
        progressDialog.dismiss();
        ApiHelper.showError(this, error, new ApiHelper.ErrorListener() {
            @Override
            public void tryAgain() {
//                sendForm();
                progressDialog.setProgress(0);
                progressDialog.show();
                ApiSimulator.createAds(getAdParams(), CreateAdDetailsActivity.this);
            }

            @Override
            public void cancel() {

            }
        });
    }

    @Override
    public void transferred(CountingTypedFile typedFile, long num, long totalSize) {
        progressDialog.setMessage(String.format("%s %d/%d", getString(R.string.sending), attachments.get(typedFile), attachments.size()));
        progressDialog.setProgress((int) ((num / (float) totalSize) * 100));
    }
}
