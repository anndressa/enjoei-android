package br.com.enjoei.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.widget.GridView;

import java.util.List;

import br.com.enjoei.R;
import br.com.enjoei.adapters.AdAdapter;
import br.com.enjoei.api.ApiSimulator;
import br.com.enjoei.api.EnjoeiApiClient;
import br.com.enjoei.helpers.ApiHelper;
import br.com.enjoei.helpers.DimensionsHelper;
import br.com.enjoei.models.Ad;
import br.com.enjoei.views.ApiContentView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends ActionBarActivity implements SwipeRefreshLayout.OnRefreshListener, Callback<List<Ad>> {

    @InjectView(R.id.api_content_view)
    ApiContentView apiContentView;

    AdAdapter adAdapter = new AdAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        setupViews();

        EnjoeiApiClient.getApiService().listAds(1, this);
    }

    public void setupViews() {
        GridView gridView = new GridView(this);
        gridView.setHorizontalSpacing(DimensionsHelper.convertToPixels(this,5));
        gridView.setVerticalSpacing(DimensionsHelper.convertToPixels(this,10));
        gridView.setNumColumns(2);
        gridView.setClipToPadding(false);
        int padding = DimensionsHelper.convertToPixels(this,15);
        gridView.setPadding(padding, padding, padding, padding);
        gridView.setAdapter(adAdapter);

        apiContentView.setContentView(gridView);
        apiContentView.setOnRefreshListener(this);
    }

    @OnClick(R.id.create_ad)
    public void createAd() {
        startActivity(new Intent(this, CreateAdImagesActivity.class));
    }


    @Override
    public void onRefresh() {
        if (adAdapter.isEmpty()) {
            apiContentView.showLoadingView();
        }
        ApiSimulator.listAds(1, this);
    }

    @Override
    public void success(List<Ad> ads, Response response) {
        adAdapter.addAll(ads);
        apiContentView.showContentView();
    }

    @Override
    public void failure(RetrofitError error) {
        apiContentView.setMessageError(ApiHelper.getMessageError(this, error));
        apiContentView.showErrorView();
    }
}
