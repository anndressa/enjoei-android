package br.com.enjoei.helpers;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

/**
 * Created by anndressa on 14/02/15.
 */
public class DimensionsHelper {

    public static int convertToPixels(Context context, int dp) {
        Resources r = context.getResources();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }
}
