package br.com.enjoei.helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import br.com.enjoei.R;
import retrofit.RetrofitError;

/**
 * Created by anndressa on 14/02/15.
 */
public class ApiHelper {

    public static interface ErrorListener {
        public void tryAgain();

        public void cancel();
    }

    public static String getMessageError(Context context, final RetrofitError error) {
        int messageRes = error != null && error.getKind() == RetrofitError.Kind.NETWORK ? R.string.error_network : R.string.error_default;
        return context.getString(messageRes);
    }

    public static void showError(Context context, final RetrofitError error, final ErrorListener listener) {
        new AlertDialog.Builder(context)
                .setTitle(R.string.error_title)
                .setMessage(getMessageError(context, error))
                .setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.tryAgain();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.cancel();
                    }
                })
                .show();
    }
}
